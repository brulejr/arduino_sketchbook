/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <Bounce2.h>
#include <Homie.h>
#include <DHT.h>

const int PIN_DHT22 = D4;
const int PIN_WATER = D1;
const int MEASUREMENT_INTERVAL = 300;

unsigned long lastMeasurementSent = 0;

DHT dht(PIN_DHT22, DHT22);

HomieNode temperatureNode("temperature", "temperature");
HomieNode humidityNode("humidity", "humidity");
HomieNode waterLeakNode("water-leak", "water-leak");

Bounce debouncer = Bounce();

void setupHandler() {
  temperatureNode.setProperty("unit").send("c");
  debouncer.attach(PIN_WATER);
  debouncer.interval(5);
}

void loopHandler() {
  debouncer.update();

  if (millis() - lastMeasurementSent >= MEASUREMENT_INTERVAL * 1000UL || lastMeasurementSent == 0) {
    float t = dht.readTemperature();
    float h = dht.readHumidity();
    int w = debouncer.read();

    Homie.getLogger() << "Temperature: " << t << " °C | humidity: " << h << "% | water: " << w << endl;
    if (!isnan(t)) {
      temperatureNode.setProperty("temperature").send(String(t));
      temperatureNode.setProperty("json").send("{\"t\":" + String(t) + ",\"unit\":\"c\"}");
      
      humidityNode.setProperty("humidity").send(String(h));
      humidityNode.setProperty("json").send("{\"h\":" + String(h) + ",\"unit\":\"%\"}");
      
      if (w == LOW) {
        waterLeakNode.setProperty("status").send(String("true"));
        waterLeakNode.setProperty("json").send("{\"w\":\"true\"");
      } else {
        waterLeakNode.setProperty("status").send(String("false"));
        waterLeakNode.setProperty("json").send("{\"w\":\"false\"");
      }
      
      lastMeasurementSent = millis();
    }
  }
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  Homie_setFirmware("wall-sensor", "1.0.0");
  Homie.setSetupFunction(setupHandler).setLoopFunction(loopHandler);

  Homie.setup();
}

void loop() {
  Homie.loop();
}
