/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Jon Brule <brulejr@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <Homie.h>
#include <DHT.h>

const int DEFAULT_MEASUREMENT_INTERVAL = 300;
const int PIN_DHT22 = D4;

unsigned long lastMeasurementSent = 0;

DHT dht(PIN_DHT22, DHT22);

HomieSetting<long> intervalSetting("interval", "Time between readings (in seconds)"); 
HomieNode temperatureNode("temperature", "temperature");
HomieNode humidityNode("humidity", "humidity");

void setupHandler() {
  temperatureNode.setProperty("unit").send("c");
}

void loopHandler() {
  if (millis() - lastMeasurementSent >= intervalSetting.get() * 1000UL || lastMeasurementSent == 0) {
    float t = dht.readTemperature();
    float h = dht.readHumidity();

    Homie.getLogger() << "Temperature: " << t << " °C | humidity: " << h << "%" << endl;
    if (!isnan(t)) {
      temperatureNode.setProperty("temperature").send(String(t));
      temperatureNode.setProperty("json").send("{\"t\":" + String(t) + ",\"unit\":\"c\"}");
      
      humidityNode.setProperty("humidity").send(String(h));
      humidityNode.setProperty("json").send("{\"h\":" + String(h) + ",\"unit\":\"%\"}");
      
      lastMeasurementSent = millis();
    }
  }
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  Homie_setBrand("BruleNet");
  Homie_setFirmware("wall-sensor", "1.0.0");
  Homie.setSetupFunction(setupHandler).setLoopFunction(loopHandler);

  intervalSetting.setDefaultValue(DEFAULT_MEASUREMENT_INTERVAL).setValidator([] (long candidate) {
    return candidate >= 0;
  });  

  Homie.setup();
}

void loop() {
  Homie.loop();
}
